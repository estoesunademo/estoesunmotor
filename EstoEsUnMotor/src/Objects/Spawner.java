package Objects;

import MiPaquete.Motor;

public class Spawner extends MonoBehaviour
{
    int interval;
    long counter;

    Prefab prefab;
    public Spawner(GameObject gameObject, Prefab prefab, int interval) {
        super(gameObject);
        this.interval = interval;
        this.prefab = prefab;
    }

    @Override
    public void update() {
        counter += Motor.getInstance().deltaTime;
        if (counter > interval) {
            counter -= interval;

            GameObject g = Motor.getInstance().instantiate(prefab);
            Motor.getInstance().log("Spawning " + prefab.toString() +" #id:" + g.id);
        }
    }

}
