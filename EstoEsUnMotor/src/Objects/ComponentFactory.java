package Objects;

public class ComponentFactory {

    public GameObject create(Prefab p, int id) {
        GameObject g = new GameObject();
        g.id = id;
        switch (p) {
            case HotDog:
                g.addComponent(new HotDog(g));
                break;
            case HotDogEmo:
                g.addComponent(new HotDog(g));
                g.addComponent(new Kms(g, 4000));
            default:
                break;
        }
        return g;
    }

}
