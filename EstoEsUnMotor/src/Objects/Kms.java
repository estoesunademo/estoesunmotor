package Objects;

import MiPaquete.Motor;

public class Kms extends MonoBehaviour{
    long dead_ms;
    long counter = 0;

    GameObject miHotdog;

    Kms(GameObject gameObject, long dead_ms){
        super(gameObject);
        this.dead_ms = dead_ms;
    }

    @Override
    void start() {
        miHotdog = Motor.getInstance().instantiate(Prefab.HotDog);
        Motor.getInstance().log("Mi hotodog tiene la id " + miHotdog.id);
    }

    @Override
    public void update() {
        counter += Motor.getInstance().deltaTime;
        Motor.getInstance().log(" ID --> " + gameObject.id + " - Counter: " + counter/1000 + "s");
        if (counter >= dead_ms) {
            Motor.getInstance().log("Me suicido " + gameObject.id +" a los " + counter/1000 + "s");
            Motor.getInstance().destroy(gameObject);
        }
    }

    @Override
    void onDestroy() {
        Motor.getInstance().destroy(miHotdog);
    }
}
