package Objects;

import MiPaquete.Motor;

public class A extends MonoBehaviour {
    public void update() {
        Motor.getInstance().log("A");
    }

    public void javadoc() {
        Motor.getInstance().log("no");
    }

    public A(GameObject gameObject) {
        super(gameObject);
    }

}
