package Objects;

import MiPaquete.Motor;

public class HotDog extends MonoBehaviour {
    public HotDog(GameObject gameObject){
        super(gameObject);
    }

    public void start() {
        Motor.getInstance().log("HotDog ID: " + gameObject.id);
        Motor.getInstance().log("I'm a new hot dog.");
        Motor.getInstance().log("HotDog ID: " + gameObject.id);
    }

    public void update() {
        //Motor.log("HotDog " + gameObject.id + " is being updated.");
    }

    public void onDestroy() {
        Motor.getInstance().log("HotDog " + gameObject.id + " is being destroyed. MiPrimerRunnable");
    }

}
