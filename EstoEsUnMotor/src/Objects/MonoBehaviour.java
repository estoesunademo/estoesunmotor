package Objects;

public abstract class MonoBehaviour {
    GameObject gameObject;
    void start() {}
    void update() {}
    void onDestroy() {}

    public MonoBehaviour(GameObject g) {
        this.gameObject = g;
    }

}
