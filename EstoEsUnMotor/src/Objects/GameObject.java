package Objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GameObject {
    public int id = -1;

    Collection<MonoBehaviour> components = new ArrayList<>();
    public void addComponent(MonoBehaviour component) {
        components.add(component);
    }

    public void removeComponent(MonoBehaviour component) {
        components.remove(component);
    }

    public <T extends MonoBehaviour> T getComponent(Class<T> type) {
        for (MonoBehaviour component : components) {
            if (type.isInstance(component)) {
                return type.cast(component);
            }
        }
        return null;
    }

    public void start() {
        for (MonoBehaviour component : components) {
            component.start();
        }
    }

    public void update() {
        for (MonoBehaviour component : components) {
            component.update();
        }
    }

    public void destroy() {
        for (MonoBehaviour component : components) {
            component.onDestroy();
        }
    }

}
