import MiPaquete.Motor;
import Objects.*;

public class Main {
    public static void main(String[] args) {
        Motor motor = Motor.getInstance();
        GameObject spawner = new GameObject();
        spawner.addComponent(new Spawner(spawner, Prefab.HotDogEmo, 2000));
        motor.instantiate(spawner);
        motor.run();
    }
}
