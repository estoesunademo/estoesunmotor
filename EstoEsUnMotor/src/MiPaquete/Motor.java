package MiPaquete;

import Objects.ComponentFactory;
import Objects.GameObject;
import Objects.Prefab;

import java.util.ArrayList;
import java.util.List;

public class Motor {
    private int fps = 2;
    private static Motor motor = null;
    public long deltaTime = 0;
    private int nextID = 0;
    public ComponentFactory factory = new ComponentFactory();
    private List<GameObject> inUse = new ArrayList<>();
    private boolean isAdding = false;
    private List<GameObject> toAdd = new ArrayList<>();
    private List<GameObject> toAddAlt = new ArrayList<>();
    private boolean isRemoving = false;
    private List<GameObject> toRemove = new ArrayList<>();
    private List<GameObject> toRemoveAlt = new ArrayList<>();

    private Motor() {}

    public static synchronized Motor getInstance() {
        if (motor == null)
            return motor = new Motor();
        return motor;
    }

    public void run() {
        while (true) {
            long startTime = now();
            add();
            update();
            remove();
            long endTime = now();
            long sleepTime = 1000 / fps - (endTime - startTime);
            sleep(sleepTime);
            deltaTime = now() - startTime;
        }
    }


    private void update() {
        for (GameObject gameObject : inUse) {
            gameObject.update();
        }
    }

    private void add() {
        while (toAdd.size() > 0 || toAddAlt.size() > 0) {
            isAdding = true;
            for (GameObject gameObject : toAdd) {
                inUse.add(gameObject);
                gameObject.start();
            }
            toAdd.clear();
            isAdding = false;
            for (GameObject gameObject : toAddAlt) {
                inUse.add(gameObject);
                gameObject.start();
            }
            toAddAlt.clear();
        }
    }

    private void remove() {
        while (toRemove.size() > 0 || toRemoveAlt.size() >0) {
            isRemoving = true;
            for (GameObject gameObject : toRemove) {
                gameObject.destroy();
                inUse.remove(gameObject);
            }
            toRemove.clear();
            isRemoving= false;
            for (GameObject gameObject : toAddAlt) {
                inUse.remove(gameObject);
            }
            toRemoveAlt.clear();
        }
    }

    public void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private long now() {
        return System.currentTimeMillis();
    }

    public void destroy(GameObject gameObject){
        if (isRemoving) {
            toRemoveAlt.add(gameObject);
        } else {
            toRemove.add(gameObject);
        }
    }

    public void instantiate(GameObject gameObject){
        gameObject.id = getId();
        if (isAdding) {
            toAddAlt.add(gameObject);
        }
        else {
            toAdd.add(gameObject);
        }
    }
    public GameObject instantiate(Prefab prefab){
        GameObject g = factory.create(prefab, getId());
        instantiate(g);
        return  g;
    }
    public int getId() {
        return nextID++;
    }

    public void log(String message){
        String time = java.time.LocalTime.now().toString().substring(0, 8);
        System.out.println("[" + time + "] " + message);
    }

}
